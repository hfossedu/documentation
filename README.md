# Kits

## Status

The Kits effort is in the early phases of development. Expect change.

## Description

A Kit is an educational tool designed to help students develop the skills
and knowledge necessary to contribute to open source projects, and more
generally to engage in modern software development.

Each Kit has a set of learning objectives, instructional materials, and
artifacts from a real project frozen at a moment in time. A Kit provides
instructors and students an environment in which they can take on the
roles of software developers and maintainers of a real project without
fear of disrupting a real project. This gives them the freedom to try
things, make mistakes, and learn. The instructional materials guide
instructors and students through prepared, repeatable activities to help
students achieve the Kit's learning objectives.

## List of Kits

* [GitKit](https://gitlab.com/hfossedu/kits/GitKit)
  * Status: Beta (ready for use by others)
  * Learning Goal: Able to contribute to FOSS projects using Git and GitHub/GitLab.
* [MicroservicesKit](https://gitlab.com/LibreFoodPantry/training/microservices/microserviceskit)
  * Status: Alpha (ready for use by team members)
  * Learning Goal: Able to develop microservices including RESTful APIs,
    frontends, and backends using Docker, OpenAPI, CI/CD, and more.
* TestingKit
  * Status: Proposed
* CICDKit
  * Status: Proposed

## Kit Ecosystem

* [KitClient](https://gitlab.com/hfossedu/kits/KitClient)
  * Status: Beta
  * Goal: Provide a complete, predictable, development environment for use with Kits.
* KitDeployer
  * Status: Planed
  * Goals: Provide instructors a single tool to deploy any Kit and simplify Kit
    specification for kit designers.
* KitCapture
  * Status: Planed
  * Goals: Provide tools for kit designers to capture artifacts of a FOSS project
    at a moment in time for inclusion in a kit.
* KitFeatures
  * Status: Planed
  * Goal: Provide a collection of features that kit designers can select from.

## Deployment Diagram

The diagram below shows the different components of a Kit and the Kit ecosystem,
where they are deployed, and end users that interact with them.

```plantuml
@startuml

node InstructorMachine {
  component KitDeploy #line.dashed; {
  }
}
cloud Git??b {
  frame HFOSSedu {
    database Kit {
    }
    database KitFeatures #line.dashed; {
    }
  }

  frame InstructorOwned {
    database DeployedKit {
    }
  }
  database Project {
  }
}

KitDeploy <-- Instructor: interacts with
Kit <-- KitDeploy: reads
DeployedKit <-- KitDeploy: creates

actor Student
node StudentMachine {
  component KitClient {
  }
}
KitClient <-- Student : interacts with
DeployedKit <-- KitClient : communicates with

actor KitDesigner
node KitDesignerMachine {
  component KitCapture #line.dashed; {
  }
}
KitDesigner --> KitCapture : interacts with
KitDesigner --> KitFeatures : selects
KitCapture --> Project : reads
KitCapture --> Kit : writes
KitDesigner --> Kit : creates
@enduml

```

The KitClient exists. The KitDeploy and KitCapture do not; that's why they have dashed lines.

## Concept Maps

The diagrams below help identify the different roles and components in the Kits
ecosystem and the relationships between them.

```plantuml
@startuml
actor KitDeveloper
card KitDeploy
card KitCapture
card KitFeatures
card KitClient

KitDeveloper -- KitDeploy : creates >
KitDeveloper -- KitCapture : creates >
KitDeveloper -- KitFeatures : creates >
KitDeveloper -- KitClient : creates >
@enduml
```

```plantuml
@startuml
actor KitDesigner
card KitCapture
card KitFeatures
card Kit
card Projects
card ProjectData
card InstructionalMaterials

KitDesigner -- Projects : selects >
KitDesigner -- InstructionalMaterials : creates >
KitDesigner -- KitCapture : uses >
KitDesigner -- KitFeatures : selects >
KitDesigner -- KitFeatures : creates >
KitDesigner -- Kit : creates >
Kit -- KitFeatures : contains >
KitCapture -- Projects : reads >
KitCapture -- ProjectData : creates >
Kit -- ProjectData : contains >
Kit -- InstructionalMaterials : contains >
@enduml
```

```plantuml
@startuml
actor Instructor
card Kit
card KitDeploy
card InstructionalMaterials
card Namespace
card KitDeployment

Instructor -- Kit : selects >
Instructor -- InstructionalMaterials : uses >
Kit -- InstructionalMaterials : contains >
Instructor -- KitDeploy : uses >
KitDeploy -- Kit : reads >
KitDeploy -- KitDeployment : deploys >
KitDeploy -- Namespace : deploys-into >
Instructor -- KitDeployment : uses >
Instructor -- Namespace : owns >
KitDeployment -- Namespace : lives-in >
@enduml
```

```plantuml
@startuml
actor Instructor
actor Student
card InstructionalMaterials
card KitDeployment
card KitClient

Student -- KitClient : uses >
Student -- InstructionalMaterials : uses >
Instructor -- KitDeployment : uses >
Instructor -- InstructionalMaterials : provides >
KitClient -- KitDeployment : connects-to >
KitDeployment -- KitClient : customizes >
@enduml
```
