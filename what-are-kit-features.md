# What are kit features?

A kit may add custom artifacts or functionality beyond what the frozen
kit artifacts or the KitClient provide. For example the GitKit provides
many additional features.

* Kit-TTY - A set of git hooks and shims that detect or prevent common
    mistakes made by students, and provides instructions of how to correct
    or avoid the mistake in the future.
* Community Simulation - GitHub workflow actions that react to events and
    perform actions that would normally be performed by community members.
* Custom labels and issues - These populate a deployment with issues for
    use with the activity.
* Custom repository branches - These are used by the instructor during the
    lesson to inject merge conflicts so that students can learn to resolve
    conflicts.

Kit features are installed at "deploy time", at "KitClient clone time", or
both. "Deploy time" means that when the kit is deployed, these features
install themselves into the deployed repositories. "KitClient clone time"
means they are installed when a KitClient clones a deployment. A feature
that installs at "KitClient clone time" only works if a KitClient clones
it. This is because a KitClient has a special hook to run a kit's
client-side installer when one of the kit's deployments is cloned.
