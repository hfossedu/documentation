# What is in a Kit?

Conceptually a kit contains an instructor guide, instructional materials,
and artifacts from a real project frozen in time. How this is accomplished
is described here.

A kit is stored in a git repository. In the repository are instructions
for instructors, learning materials, a deployment definition, and project
data.

The deployment definition describes the artifacts that should be deployed
and any features that should be installed into a deployment and/or any
KitClient that interacts with a deployment.

The project data stored in a kits repository is data collected from the
project at a moment in time. This data is used to reproduce the projects
artifacts in a deployment of the kit.

Not all artifacts are represented as complete data. For example, a
complete clone of repositories for a project need not be stored in the
kit. Instead, a kit can store a clone URL for each repo to be captured
along with a list of branch names and commit IDs for each branch. With
this data, repositories with the specified branch reset to the given
commit ID can be created. The advantage to this approach is that the
kit repository does not have to store entire repositories from other
projects. The disadvantage is that if project repositories are ever moved,
renamed, deleted, or commits are ever squash (effectively deleted), then
the kit will not longer be able to reproduce those repositories.

Data that is frequently stored in a kit's repository include issue tracker
data (e.g., labels, issues, comments, MRs/PRs, etc.). That's because the
state of an issue at a given point in time may be hard to reproduce. For
example, if the description of an issue has been modified since it was
captured. Also, issue tracker data may be treated less sacredly as a
repositories history; and may be deleted.

> **NOTE**
>
> At the time of writing (20230318), GitKit (the first kit) contain
> deployment scripts which are containerized into a Docker container so
> that instructors can deploy GitKit with a single Docker command. It
> also contains scripts that implement features for the deployment
> and/or KitClient.
>
> The plan is to extract and generalize these scripts into a separate
> KitDeploy project that is capable of deploying any kit that defines
> its deployment in a predefined format. Also, we want to factor out the
> implementation of features into a separate project so that other kits
> can use them by naming and configuring the features they want included.
