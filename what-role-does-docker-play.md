# What role does Docker play?

Students and instructors need a single software dependency installed on
their machine to deploy and interact with a kit: Docker.

> **VNC**
>
> Optionally, one may want to install a VNC client (e.g., TigerVNC). It
> improves the user experience when using the KitClient.

This doesn't mean that a kit or its deployments are Dockerized themselves
(although they could be). It just means that the components that make up
the interface for instructors and their students are provided as Docker
containers.

## Deployment

Currently (20230318), GitKit creates a Docker image for deploying GitKit.
This allows instructors (or students) to deploy GitKit with a single
Docker command. This Docker image does not contain any part of the GitKit
or its frozen project (FarmData2). It provides a runtime environment that
contains all the dependencies needed by the deploy scripts inside GitKit.
When it is ran, it clones the GitKit and calls its top-level deploy script
which does the rest of the work.

## KitClient

The KitClient is a Docker image that contains a GUI, linux environment
installed with some common developer tools (e.g., Git, VSCodium, etc.).
It's purpose it to provide students with a standard "machine" for
interacting with kits. This allows kits to be designed against a known,
safe, customizable interface. "Known" is important so that instructions
can provide students with accurate, precise instructions. It's "safe" in
that changes within a KitClient container cannot hurt anything outside
the container; this empowers students to explore, experiment, and learn.
It's "customizable" in that other applications can be install, it can be
configured, just like a normal Linux machine. Kits can take advantage of
this to further customize a KitClient that interacts with its deployments
to install other tools, helpers, etc. In fact, when a kit deployment is
cloned into a KitClient, the KitClient automatically calls an install
script in the script allowing the kit to customize the KitClient.
